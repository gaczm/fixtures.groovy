import bean.in.some.packkage.Bean

import static GenericCreator.create

class GenericCreator {
    static <T> T create(Class<T> clazz, @DelegatesTo(T) Closure<?> initializer) {
        create(clazz.newInstance(), initializer)
    }

    static <T> T create(T instance, @DelegatesTo(T) Closure<?> initializer) {
        initializer.setResolveStrategy(Closure.DELEGATE_FIRST)
        initializer.setDelegate(instance)
        initializer.call()

        return instance
    }
}

class Creator {

    static SomeBean someBean(@DelegatesTo(SomeBean) Closure<?> initializer) {
        create(SomeBean, initializer)
    }

    static SomeBean someBean(SomeBean instance, @DelegatesTo(SomeBean) Closure<?> initializer) {
        create(instance, initializer)
    }

    static Bean bean(@DelegatesTo(Bean) Closure<?> initializer) {
        create(Bean, initializer)
    }

    static Bean bean(Bean instance, @DelegatesTo(Bean) Closure<?> initializer) {
        create(instance, initializer)
    }
}

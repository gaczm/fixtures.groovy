package bean.in.some.packkage;

public class Bean {
    private int xyz, abc;
    private String def, ghi;

    private Bean() {
    }

    public Bean(int abc, String def) {
        this.abc = abc;
        this.def = def;
    }

    public int getXyz() {
        return xyz;
    }

    public void setXyz(int xyz) {
        this.xyz = xyz;
    }

    public int getAbc() {
        return abc;
    }

    public void setAbc(int abc) {
        this.abc = abc;
    }

    public String getDef() {
        return def;
    }

    public void setDef(String def) {
        this.def = def;
    }

    public String getGhi() {
        return ghi;
    }

    public void setGhi(String ghi) {
        this.ghi = ghi;
    }

    @Override
    public String toString() {
        return "Bean{" +
                "xyz=" + xyz +
                ", abc=" + abc +
                ", def='" + def + '\'' +
                ", ghi='" + ghi + '\'' +
                '}';
    }
}

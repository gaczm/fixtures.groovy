import bean.in.some.packkage.Bean;

import java.util.Collection;

public class SomeBean {
    private String someString1;
    private String someString2;
    private String someString3;
    private String someString4;
    private String someString5;

    private int someInt1;
    private int someInt2;
    private int someInt3;
    private int someInt4;

    private Bean bean;
    private Collection<SomeBean> other;

    private SomeBean() {

    }

    public SomeBean(String someString5, int someInt1) {
        this.someString5 = someString5;
        this.someInt1 = someInt1;
    }

    public SomeBean(String someString5, String someString4, int someInt3, String someString1) {
        this.someString5 = someString5;
        this.someString4 = someString4;
        this.someInt3 = someInt3;
        this.someString1 = someString1;
    }

    public String getSomeString1() {
        return someString1;
    }

    public void setSomeString1(String someString1) {
        this.someString1 = someString1;
    }

    public String getSomeString2() {
        return someString2;
    }

    public void setSomeString2(String someString2) {
        this.someString2 = someString2;
    }

    public String getSomeString3() {
        return someString3;
    }

    public void setSomeString3(String someString3) {
        this.someString3 = someString3;
    }

    public String getSomeString4() {
        return someString4;
    }

    public void setSomeString4(String someString4) {
        this.someString4 = someString4;
    }

    public String getSomeString5() {
        return someString5;
    }

    public void setSomeString5(String someString5) {
        this.someString5 = someString5;
    }

    public int getSomeInt1() {
        return someInt1;
    }

    public void setSomeInt1(int someInt1) {
        this.someInt1 = someInt1;
    }

    public int getSomeInt2() {
        return someInt2;
    }

    public void setSomeInt2(int someInt2) {
        this.someInt2 = someInt2;
    }

    public int getSomeInt3() {
        return someInt3;
    }

    public void setSomeInt3(int someInt3) {
        this.someInt3 = someInt3;
    }

    public int getSomeInt4() {
        return someInt4;
    }

    public void setSomeInt4(int someInt4) {
        this.someInt4 = someInt4;
    }

    public Bean getBean() {
        return bean;
    }

    public void setBean(Bean bean) {
        this.bean = bean;
    }

    public Collection<SomeBean> getOther() {
        return other;
    }

    public void setOther(Collection<SomeBean> other) {
        this.other = other;
    }

    @Override
    public String toString() {
        return "SomeBean{" +
                "someString1='" + someString1 + '\'' +
                ", someString2='" + someString2 + '\'' +
                ", someString3='" + someString3 + '\'' +
                ", someString4='" + someString4 + '\'' +
                ", someString5='" + someString5 + '\'' +
                ", someInt1=" + someInt1 +
                ", someInt2=" + someInt2 +
                ", someInt3=" + someInt3 +
                ", someInt4=" + someInt4 +
                ", bean=" + bean +
                ", other=" + other +
                '}';
    }
}

import static Creator.bean
import static Creator.someBean

class SomeBeanFixtures {

    static bean1 = someBean {
        someInt3 = 3
        someString1 = "ala"
        someInt4 = new Date().time.toInteger()
    }

    static bean2 = someBean(new SomeBean("ala", 123)) {
        someString1 = "ula"

        bean = bean {
            xyz = 123
        }

        other = [
                someBean {
                    someInt3 = 1
                    someInt4 = 8

                    other = [
                            someBean {
                                other = [
                                        someBean {
                                            someString1 = "ala ma kota"
                                        }
                                ]
                            }
                    ]
                },
                someBean(new SomeBean("ala", 901)) {
                    bean = Creator.bean {
                        abc = 9913
                    }
                }
        ]
    }

    static b3 = bean {
        xyz = 123
        abc = bean2.other[0].someInt4
    }

    static b4 = GenericCreator.<SomeBean>create(SomeBean.class) {
        someInt4 = 123
    }

    // -----------------------------------------------------------------

    static void main(String[] args) {

        println bean1
        println bean2
        println b3

    }

}


import org.apache.commons.io.FileUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;

public class FixturesCreatorGenerator {

    private File path;
    private Collection<String> foundClasses;

    // -----------------------------------------------------------------

    private static final String BEFORE_TEMPLATE =
        "import groovy.transform.CompileStatic\n" +
        "@CompileStatic\n" +
        "class GenericCreator {\n" +
        "   static <T> T create(Class<T> clazz, Closure<?> initializer) {\n" +
        "       def instance = clazz.newInstance()\n" +
        "       initializer.setResolveStrategy(Closure.DELEGATE_ONLY)\n" +
        "       initializer.setDelegate(instance)\n" +
        "       initializer.call()\n" +
        "       return instance\n" +
        "   }\n";

    /**
     * Class, class, Class, Class
     */
    private static final String ENTRY_TEMPLATE =
        "   static %s %s(@DelegatesTo(%s) Closure<?> initializer) {\n" +
        "       create(%s, initializer)\n" +
        "   }\n";

    private static final String AFTER_TEMPLATE =
        "}\n";

    // -----------------------------------------------------------------

    public FixturesCreatorGenerator(File path) {
        this.path = path;
    }

    public void generateTo(File outputFile) {
        Collection<File> files = FileUtils.listFiles(path, new String[]{"java"}, true);
        foundClasses = new ArrayList<>(files.size());

        System.out.println(files.size());

        for (File clazz : files) {
            String name = clazz.getName();
            name = name.substring(0, name.length() - 5);
            foundClasses.add(name);
        }

        save(outputFile);
    }

    private void save(File outputFile) {
        try (Writer writer = new BufferedWriter(new FileWriter(outputFile))) {
            writer.write(BEFORE_TEMPLATE);
            for (String clazz : foundClasses) {
                writer.write(String.format(ENTRY_TEMPLATE, clazz, withFirstLetterSmall(clazz), clazz, clazz));
            }
            writer.write(AFTER_TEMPLATE);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String withFirstLetterSmall(String clazz) {
        return clazz.substring(0, 1).toLowerCase() + clazz.substring(1);
    }

    public static void main(String[] args) {
        new FixturesCreatorGenerator(new File("/home/gk/Kaczmarczyk_Grzegorz_4")).generateTo(new File("/tmp/out.groovy"));
    }
}
